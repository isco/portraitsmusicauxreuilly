+++
prenom = "Caroline"
image = "boutons/bouton1/1Colonne_06.jpg"
portrait = "img/portraits/06-caroline-portrait.png"
audio = "audios/06-caroline-audio.mp3"
+++
Entre la photo et ce texte, l’épisode du confinement nous a fait vivre à tous une expérience bien particulière. Sans pouvoir me promener au bord de l’eau comme j’aime, j’ai navigué sur le fil de mon souffle pour continuer à respirer le courant de la vie