+++
prenom = "Raphaël"
image = "boutons/bouton1/1Colonne_24.jpg"
portrait = "img/portraits/24-raphael-portrait.png" 
audio = "audios/24-raphael-audio.mp3"
+++
L’espérer, c’est le perdre

Le manquer, c’est le perdre

L’avoir, c’est le perdre 

Le lâcher, c’est le préserver 