+++
prenom = "Mariam"
image = "boutons/bouton1/1Colonne_19.jpg"
portrait = "img/portraits/19-mariam-portrait.png"
audio = "audios/19-mariam-audio.mp3"
+++
un champ très vaste
joyeux et profond à la fois
des baobabs
et moi au milieu
comme une reine d’Afrique
majestueuse