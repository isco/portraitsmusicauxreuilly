+++
prenom = "RaphaelleD"
image = "boutons/bouton1/1Colonne_25.jpg"
portrait = "img/portraits/25-raphaelleD-portrait.png"
audio = "audios/25-raphaelleD-audio.mp3"
+++
La vie.. Cette valse viennoise qui vous égare parfois, mais vous promet toujours que le bonheur est proche.
On avance,on explore, on s'amuse de soi.
et la Lumière est là, allons-y , n'ayons crainte