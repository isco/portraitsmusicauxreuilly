+++
prenom = "Michelle"
image = "boutons/bouton1/1Colonne_21.jpg"
portrait = "img/portraits/21-michelle-portrait.png"
audio = "audios/21-michelle-audio.mp3"
+++
                            Rêverie

Balade au Parc Montsouris le dimanche matin, cheveux au vent, chaudement vêtue, je suis bien…

L’air frais du matin me donne du tonus et du punch.

Soudainement, spontanément, les grands espaces et le ciel m’appartiennent, rien qu’à moi…

Je me délecte de cette nature magnifiquement belle qui s’offre le cœur grand ouvert.

Les arbres me font un clin d’œil très sympathique, me sourient gentiment et agréablement, me comblent de bonheur et de joie de vivre…

Sur le plan d’eau scintillant, les canards habillés de leur plus beau plumage vert tourbillonnent, se pavanent et me saluent.

Le cygne au bec rouge a mis sa jolie robe noire plissée pour me recevoir.

La cascade déverse son eau claire et limpide ; elle m’aperçoit, me fait signe de me rapprocher.

Ici et là, mesdemoiselles Jonquilles et Tulipes ont revêtu leur jupe jaune et commencent à flirter avec leur nouveau compagnon le soleil.

Mes amis, les oiseaux aux ailes blanches m’accueillent,
m’invitent chaleureusement à leur danse, emportée
par la danse, JE M’ENVOLE…