+++
prenom = "IsabelleT"
image = "boutons/bouton1/1Colonne_15.jpg"
portrait = "img/portraits/15-isabelleT-portrait.png"
audio = "audios/15-isabelleT-audio.mp3"
+++

Ces moments du passé à tes côtés
Mes moindres traits qui sont les tiens
Tes yeux m’offrant le plus doux des sentiments…
mais tout bascule en un instant