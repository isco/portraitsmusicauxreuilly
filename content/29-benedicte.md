+++
prenom = "Bénédicte"
image = "boutons/bouton1/1Colonne_29.jpg"
portrait = "img/portraits/29-benedicte-portrait.png"
audio = "audios/29-benedicte-audio.mp3"
+++
Nos nuits visitées de tumultes, d’orages, de peurs

Nos mêmes nuits éclairées d’étincelles, de lumière, d’étoiles
joyeuses et scintillantes

Nos existences dont la nuit n’aura jamais le dernier mot

Nos existences livrées à la Lumière qui éclairera toute chose. Arc-en-ciel toujours en attente