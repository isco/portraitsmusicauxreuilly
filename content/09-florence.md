+++
prenom = "Florence"
image = "boutons/bouton1/1Colonne_09.jpg"
portrait = "img/portraits/09-florence-portrait.png"
audio = "audios/09-florence-audio.mp3"
+++

Au travers d’un rythme puissant et d’un chant lyrique se cache un monde féérique, paisible, lumineux, presque blanc, voire transparent. Un univers étrange mais pas étranger.
La puissance sonore déjoue-t-elle l’apparence de l’image ?