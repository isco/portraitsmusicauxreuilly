+++
prenom = "Hélène"
image = "boutons/bouton1/1Colonne_10.jpg"
portrait = "img/portraits/10-helene-portrait.png"
audio = "audios/10-helene-audio.mp3"
+++
Dans le sombre, avancer pas à pas
Enveloppée de rouge, 
À la lumière de la bougie
Indiquer le chemin
L'éclaireur repère les lieux
Guidée par cette lumière ténue
Tel l'enfant quittant la matrice
Pour arriver entre leurs mains

Chemin accidenté
Lumières chaleureuses
L'aventure
Avancer, avancer
Se retourner
Tout le monde est là ?
Jamais jamais nous enveloppe et nous révèle