+++
prenom = "Anne-Solenn"
image = "boutons/bouton1/1Colonne_35.jpg"
imageModal = "img/imgModal/image-modal4.jpg"
portrait = "img/portraits/35-anne-solenn-portrait.png"
audio = "audios/35-marie-aude-audio.mp3"
+++
                       Chercher ailleurs

Alors que les mots des patients sont lourds d’émotion, de peur, de douleur, je m’en vais chercher ailleurs la parole, l’image que je vais leur tendre comme un fil pour les relier à la vie, même lorsque celle-ci se délite en pointillé....

Mon regard alors part ailleurs, par-dessus mes lunettes, hors cadre,
toujours hors cadre....