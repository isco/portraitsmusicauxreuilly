+++
prenom = "Juliette"
image = "boutons/bouton1/1Colonne_33.jpg"
imageModal = "img/imgModal/image-modal3.jpg"
portrait = "img/portraits/33-juliette-portrait.png"
audio = "audios/33-juliette-audio.mp3"
+++
J'embrasse avec ardeur les plaisirs que le temps me laisse
L'heure qui passe lentement pleure la chanson du souvenir