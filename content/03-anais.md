+++
prenom = "Anaïs"
image = "boutons/bouton1/1Colonne_03.jpg"
portrait = "img/portraits/03-anais-portrait.png"
audio = "audios/03-anais-audio.mp3"
+++

Février 2020, rencontre avec Sophie et Laurent.
Assise dans un tipi, lui-même construit à l’abri d’une
chapelle souterraine, grâce à un air d’accordéon et de piano, je pars en voyage. Je me retrouve plongée au coeur de l’océan, tour à tour tumultueux puis extrêmement doux.
Je sens le sel, le vent, le sable… Alors même qu’il est composé par deux inconnus, ce portrait musical me donne
l’impression de me redécouvrir : à travers leurs notes, ils peignent une personne plus forte, plus solide, plus confiante que celle que je suis. Cette version de moi est-elle celle que je rêverais d’être ou celle que je n’ose
pas être ?
