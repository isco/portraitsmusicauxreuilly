+++
prenom = "Corinne"
image = "boutons/bouton1/1Colonne_07.jpg"
portrait = "img/portraits/07-corinne-portrait.png"
audio = "audios/07-corinne-audio.mp3"
+++

Au bord de l’eau, je me promène. Légèreté, insouciance et délicatesse ! Ces trois mots, à eux seuls, s’imposent dans ma bulle et résument mon allégresse. S’est envoyé une carte postale depuis cet espace temps-là. 