+++
prenom = "Laurence"
image = "boutons/bouton1/1Colonne_18.jpg"
portrait = "img/portraits/18-laurence-portrait.png"
audio = "audios/18-laurence-audio.mp3"
+++
D’abord l’odeur des bébés….quand on enfouit son nez dans le cou de son bébé, cette odeur que l’on reconnaitra toujours entre mille, inimitable, sucrée et porteuse de tant d’émotions. Ils grandiront, l’odeur changera mais le souvenir toujours restera des câlins de l’enfance.
Puis l’odeur de la brioche dans toute la maison, amenant la salive à la bouche, ouvrant l’appétit. La machine à
pain fait son œuvre et répand son arôme, la brioche sera prête pour le petit déjeuner demain matin, mais en attendant tout le monde en croquerait bien déjà, petits et grands. 

L’odeur des roses, les roses pourpres du jardin de Saint-Claude, d’un vieux rosier hors d’âge. Les pétales sont épais comme du velours d’un rouge sombre et profond, l’odeur
s’exhale le soir à la brume, capiteuse, envoutante. Tout l’été, le jardin embaumera. 

D’autres odeurs de la nature viennent ensuite, le sous-bois après la pluie, la mer et son parfum iodé, le tilleul en fleur dans la cour, le chèvrefeuille croisé au cours d’une
promenade qui court le long d’un mur, la fumée de la cheminée lorsqu’on l’allume aux premiers froids, le feu de bois et la tourbe comme dans un single malt écossais.

Enfin, les odeurs liées au travail. Remugles de vomissements mêlés aux odeurs corporelles, désinfectant et transpiration, souvenir des souffrances des patients dans le face à face qui nous rapprochait. Et puis, l’odeur indéfinissable des couloirs des chambres de garde, ces longs couloirs obscurs, déserts, mal éclairés, souvent au fond d’un bâtiment antédiluvien, mélange de poussière, d’humidité et de renfermé, que je retrouve encore parfois à l’AP-HP
et qui ne parle qu’à moi quand on va se coucher tard, seule,
épuisée, dans la nuit.