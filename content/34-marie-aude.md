+++
prenom = "Marie-Aude"
image = "boutons/bouton1/1Colonne_34.jpg"
imageModal = "img/imgModal/image-modal4.jpg"
portrait = "img/portraits/34-marie-aude-portrait.png"
audio = "audios/34-marie-aude-audio.mp3"
+++
Se laisser entraîner par les notes pour se découvrir, prendre le temps d’un voyage intérieur : fermer les yeux pour mieux se voir.