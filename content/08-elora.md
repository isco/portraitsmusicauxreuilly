+++
prenom = "Élora"
image = "boutons/bouton1/1Colonne_08.jpg"
portrait = "img/portraits/08-elora-portrait.png"
audio = "audios/08-elora-audio.mp3"
+++

Il a suffi de se laisser porter par les instruments pour replonger dans ma tête. Cette tête qui chaque jour doit prendre de multiples décisions. La musique résonnait comme un échange entre plusieurs petites voix dans ma tête qui discutent, se contredisent et dialoguent pour prendre des décisions.
