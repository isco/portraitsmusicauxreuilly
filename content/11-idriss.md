+++
prenom = "Idriss"
image = "boutons/bouton1/1Colonne_11.jpg"
portrait = "img/portraits/11-idriss-portrait.png"
audio = "audios/11-idriss-audio.mp3"
+++
Je ne me souviens plus très bien, je dirais même
que c’est assez flou. Pourtant je fais ce trajet depuis toujours, c’est ma trajectoire du point de départ vers le point d’arrivée. Et ce jour-là, je crois que j’ai changé de voie sans même m’en rendre compte.
Pourtant j’arrive, j’y arrive. 