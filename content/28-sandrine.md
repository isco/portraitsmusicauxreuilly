+++
prenom = "Sandrine"
image = "boutons/bouton1/1Colonne_28.jpg"
portrait = "img/portraits/28-sandrine-portrait.png" 
audio = "audios/28-sandrine-audio.mp3"
+++
Comme cette prairie me paraît vaste !
Emplie de verdure, mes yeux s’égarent, je tremble, j’ai froid,
Mais je marche droit devant moi.
Le vent me porte, me guide je ne sais où,
qu’importe,j’ai confiance.
Bercé de lumière et d’ombres,
Il m’appelle, ses bras m’enveloppent.
Il est là, seul au milieu de nulle part.
Je le touche doucement, le frôle du bout des doigts,
Ma peur, je la sens qui s’envole,
Je m’assois, m’adosse à lui et m’imprègne.
Mes yeux se ferment, mes sens s’éveillent.
Comme je suis bien, légère comme une feuille, 
Comme je me sens bien.
Protégée, envahie de douceur, 
Eloignées de moi, ces mauvaises vibrations,
Au loin ce tumulte, ces mots incompris, 
Ces maux douloureux.
A la fois si loin et si proche à la fois.
Ces murmures dans ses feuillages,
Je suis là où je veux être, 
Je suis là où je dois être.
A tes pieds, toi mon protecteur
Le sauveur de mon âme,
Si Majestueux.
Comme je suis bien.
Comme tu es beau.
Merci.