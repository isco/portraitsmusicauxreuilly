+++
prenom = "Julien"
image = "boutons/bouton1/1Colonne_16.jpg"
portrait = "img/portraits/16-julien-portrait.png"
audio = "audios/16-julien-audio.mp3"
+++
Mon idée était de retrouver l’esprit musical des années 70/80 à travers une pochette de vinyle imaginaire, qui sobrement met en avant le punk-rock et toute son énergie.
Tout ça associé à un fond vert qui rappelle mon attachement à la nature et son bien-être