+++
prenom = "Laure"
image = "boutons/bouton1/1Colonne_17.jpg"
portrait = "img/portraits/17-laure-portrait.png"
audio = "audios/17-laure-audio.mp3"
+++
En cette période un peu difficile j’ai trouvé la musique qui m’était proposée assez grave ; elle reflétait sans doute mon état d’âme en cette période Covid.
Alors pour le portrait photographique nous avons cherché à amener tout ce qui pourrait me faire du bien :
- Du vert et du bleu, deux couleurs qui me font penser aux natures que j’aime : les champs bretons en bord de mer ou les cimes des sapins sur un ciel d’été
- Des personnes en pagaille : famille, amis, collègues… sans eux je ne suis rien
- Un peu de soleil sur le visage et la possibilité de regarder vers le haut, vers l’avant… pour continuer à avancer avec enthousiasme