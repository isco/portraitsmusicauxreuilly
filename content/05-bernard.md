+++
prenom = "Bernard" 
image = "boutons/bouton1/1Colonne_05.jpg"
portrait = "img/portraits/05-bernard-portrait.png"
audio = "audios/05-bernard-audio.mp3"
+++

                    Le parfum de la boulangerie

Huit heures. Je file en scooter à l’hôpital, mon travail. Il fait froid. Un peu de brouillard, mais la route est sèche. Des parents accompagnent déjà leurs enfants à l’école. Ça y est, j’ai les mains froides. Je pense à cette foutue collecte de fonds. Cette année, c’est un million qu’il faut trouver… un million ! Les soucis arrivent, se croisent. Mon scooter me conduit sans moi. Je vois au loin le feu près du garage Renault qui vient de passer au vert. J’accélère
à fond pour dépasser la file et profiter de ce créneau.
Et là, je suis soudainement traversé par l’odeur d’une
boulangerie. Tout s’apaise, tout s’efface en un instant. Ce n’est pas l’odeur du pain chaud, pas celle des croissants, pas celle des pains au chocolat ni celle des bonbons, mais celle de la boulangerie. Subtil mélange, chaud et légèrement
acidulé. En croisant d’autres enfants, je me revois petit avec Nicole, ma cousine, achetant « un pain moulé et un paquet de biscottes Clément ». Une grande responsabilité confiée par ma mère. Je nous vois revenir gaiement
dans cette petite rue, le pain à la main, croquer coupablement le croûton délicieux et imaginer un quelconque mensonge à ce sujet : « C’est un lapin qui l’a mangé !»
Nous regardions toujours des lapins dans un jardin par un petit trou entre les briques rouges du mur qui longeait le trottoir. Toute la rue était baignée par cette odeur unique de boulangerie, parfum simple et léger d’un bonheur insouciant. Où suis-je ? J’ai raté la bretelle vers l’autoroute. Pas grave, je passerai par Nogent. Ça
ira, mon rendez-vous peut attendre un peu et puis ce n’est
pas lui qui donnera un million ! En croisant d’autres
boulangeries, je ralentis et je hume, espérant retrouver le bel instant de tout à l’heure. Mais rien de pareil. C’est
certain, en rentrant ce soir, j’irai acheter mon pain dans cette nouvelle boulangerie près du garage Renault, et j’appellerai Nicole. 
