+++
prenom = "Ouarda"
image = "boutons/bouton1/1Colonne_23.jpg"
portrait = "img/portraits/23-ouarda-portrait.png" 
audio = "audios/23-ouarda-audio.mp3"
+++
Portraits croisés, histoires de vie, échanges....
Un instant un peu hors du temps et de l'espace.
Surprenant, intéressant..... à vivre… 