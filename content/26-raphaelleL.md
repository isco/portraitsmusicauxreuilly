+++
prenom = "RaphaelleL"
image = "boutons/bouton1/1Colonne_26.jpg"
portrait = "img/portraits/26-raphaelleL-portrait.png"
audio = "audios/26-raphaelleL-audio.mp3"
+++
Sanguinet. Les épines de pins jonchent le sol aride. L’odeur qui s’en dégage est comme un million d’étoiles qui me piquent le nez. Au loin, j’entends les rires joyeux de la tablée. Une partie de cartes en cours qui n’en finit plus d’amuser. Des pignons dans la salade, du magret de canard, une tourte landaise. La fumée des Gauloise sans filtre de mon père.