+++
prenom = "Fabienne"
image = "boutons/bouton1/1Colonne_32.jpg"
imageModal = "img/imgModal/image-modal2.jpg"
portrait = "img/portraits/32-fabienne-portrait.png"
audio = "audios/32-fabienne-audio.mp3"
+++
Réveil nocturne
La tristesse et l'angoisse me guettent, tapies au creux de mes draps, de mes bras
Folle étreinte pesante
Eveil, respiration
Je monte sur le ring, la joute commence
Elles m'enveloppent, dansent autour de moi
J'avance droit devant sans fléchir, pas d'esquive, la tête haute
Elles s'éloignent,
J'ai gagné le combat,
Me rendors,
Paisiblement.