+++
prenom = "Alexandre"
image = "boutons/bouton1/1Colonne_31.jpg"
imageModal = "img/imgModal/image-modal1.jpg"
portrait = "img/portraits/31-alexandre-portrait.png"
audio = "audios/31-alexandre-audio.mp3"
+++
Révolution... la serpenta canta.
Quelques pas, par brassées de secondes en arrière entre du silence sculpté par les disques du soleil (levant) et de l’acier et l’épaisse fumée rouge décibel de l’iguane.