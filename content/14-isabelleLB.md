+++
prenom = "IsabelleLB"
image = "boutons/bouton1/1Colonne_14.jpg"
portrait = "img/portraits/14-isabelleLB-portrait.png"
audio = "audios/14-isabelleLB-audio.mp3"
+++
Tout autour de moi je sens la joie, la vie. J’aime ressentir l’explosion et l’énergie qu’il y a en moi, comme un cheval galopant le long du chemin de sable blanc en bord de mer.