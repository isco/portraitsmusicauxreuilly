+++
prenom = "IsabelleL"
image = "boutons/bouton1/1Colonne_13.jpg"
portrait = "img/portraits/13-isabelleL-portrait.png"
audio = "audios/13-isabelleL-audio.mp3"
+++
Tout autour de moi je sens la joie, la vie. J’aime ressentir l’explosion et l’énergie qu’il y a en moi, comme un cheval galopant le long du chemin de sable blanc en bord de mer.
