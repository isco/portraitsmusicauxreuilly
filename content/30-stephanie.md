+++
prenom = "Stéphanie"
image = "boutons/bouton1/1Colonne_30.jpg"
portrait = "img/portraits/30-stephanie-portrait.png"
audio = "audios/30-stephanie-audio.mp3"
+++
Ici à l’hôpital, au printemps, à partir de 3h du matin, on entend les oiseaux. Je travaille de nuit. La nuit c’est calme, c’est apaisant. Comme il y a beaucoup d’arbres, les oiseaux viennent dans le jardin, au moment où Paris est calme - et eux ils chantent. 