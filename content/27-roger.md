+++
prenom = "Roger"
image = "boutons/bouton1/1Colonne_27.jpg"
portrait = "img/portraits/27-roger-portrait.png"
audio = "audios/27-roger-audio.mp3"
+++
Mélopée chamanique aux accents mineurs,

Face au feu grimé, le masque se cache,

Derrière le visage, et derrière l’ombre 