+++
prenom = "Alberto"
image = "boutons/bouton1/1Colonne_02.jpg"
portrait = "img/portraits/02-alberto-portrait.png"
audio = "audios/02-alberto-audio.mp3"
+++

Au soir de ta vie, ta plus grande satisfaction viendra de l’amour que tu auras eu pour les autres, de la manière dont tu les auras aimés. Et tu seras jugé là-dessus.
